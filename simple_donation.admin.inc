<?php

function simple_donation_settings() {
  $form = array();

  $form['simple_donation_amounts'] = array(
    '#type' => 'textarea',
    '#title' => t('Donation amounts'),
    '#description' => t('Integer amounts, one per line. A value of -1 will be used as an "Other" option.')
      . "\n" . t('Labels will be formatted using store currency. hook_simple_donation_options_alter() can be used to alter the values/labels.'),
    '#default_value' => variable_get('simple_donation_amounts', "500\n1000\n2500\n5000\n-1"),
  );
  $form['simple_donation_other'] = array(
    '#type' => 'textfield',
    '#title' => t('"Other" label'),
    '#description' => t('Label to use for the "Other" option. It will be passed through t().'),
    '#default_value' => variable_get('simple_donation_other', 'Other'),
  );
  return system_settings_form($form);
}
