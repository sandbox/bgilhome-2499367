<?php

// Alter options for donation options field
// Can include an option with value 'other'
function hook_simple_donation_options_alter(&$options) {
  // could eg. get currency and provide different options
  $currency = commerce_default_currency();
  switch ($currency['currency_code']) {
    case 'AUD':
      $options = array(
        '500' => '$5 - just a little bit',
        'other' => t('Other amount'),
      );
  }
}
